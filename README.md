# IOS

A new beginning of learning IOS development

## RESOURCES
1. UDEMY : IOS & SWIFT - The Complete iOS App Development Bootcamp by Dr. Angela Yu
2. <a href="https://colorhunt.co">colorhunt.com</a> : Select designers chosen color pallets
3. <a href="https://www.paintcodeapp.com/news/ultimate-guide-to-iphone-resolution">iPhone Resolution</a> : Get to know the Screen sizes
4. <a href="https://appbrewery.com/p/ios-course-resources">Imp Resources</a> : Resources recommended by Angela Yu

# Image creation
1. Start by creating 3X : 300 X 300 px
2. Scale down to 200X200 and 100X100 px
## Tools
1. Illustrator
2. Sketch
3. <a href="https://appicon.co">appicon.co</a> - ImageSets

# App Icon Creation
1. <a href="https://appicon.co">appicon.co</a> - AppIcon
2. Easiest way : <a href="https://canva.com">canva.com</a> and createa 1024X1024 px custom design.

# Releasing app
There are three ways. Simulator, Wired, and Wireless
1. Simulator - Straight forward
2. Physical device, Wired
    1. Matching ios and xcode version
    2. Setup developer account in xcode
        - xcode -> preferences -> accounts -> add apple id
    3. Select project -> Targets -> App Name -> Signing & Capabilities
        - check "Automatically manage signing"
        - Team -> select the account you just added
        - Error could be due to the iphone not connected with a cable to the laptop
3. Physical device, Wireless
    1. Connect the iphone with a wire
    2. XCode -> Window -> Devices and Simulators 
    3. Connect via Network checkbox must be checked (Both mac and iphone must be connected to the same network)
    4. Un-Plug your phone and you should see your phone in the select phone place.
    

